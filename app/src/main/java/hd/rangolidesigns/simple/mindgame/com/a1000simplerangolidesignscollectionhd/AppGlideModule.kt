package hd.rangolidesigns.simple.mindgame.com.a1000simplerangolidesignscollectionhd


import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class AppGlideModule : AppGlideModule()