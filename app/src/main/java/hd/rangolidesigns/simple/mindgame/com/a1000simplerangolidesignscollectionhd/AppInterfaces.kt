package hd.rangolidesigns.simple.mindgame.com.a1000simplerangolidesignscollectionhd

interface AppInterfaces {
    fun loadItem()
    fun loadSplashScreen()
    fun loadStartScreen()
    fun loadImageTopics()
    fun loadMenus()
    fun loadTestModeScreen()
    fun loadBookMarkMenu()
    fun loadBookMarkItem()
    fun loadPrivacyPolicy()


}