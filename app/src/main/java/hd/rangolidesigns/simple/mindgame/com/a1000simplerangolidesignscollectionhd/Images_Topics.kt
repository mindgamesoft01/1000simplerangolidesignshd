package hd.rangolidesigns.simple.mindgame.com.a1000simplerangolidesignscollectionhd

object Images_Topics {
    val topic_count = 10 // THE TOTAL TOPIC COUNT
    val topic_titles = arrayOf("Dotted Rangoli Designs","Free Hand Rangoli Designs","Sanskar Rangoli Designs","The Simple Dotted Rangoli Designs","The Five Parallel Dots Designs","The Dot Rangoli Design For Dewali","The Rangoli Designs For Diwali","The Dotted Floral Mehndi Designs","The Small Dot Rangoli Deigns","Simple Free Hand Rangoli Designs ") // TOPIC TITLES HERE
    val topic_icons = arrayOf("img001.webp","img002.webp","img003.webp","img004.webp","img005.webp",
            "img006.webp","img007.webp","img008.webp","img009.webp","img010.webp") //TOPIC TITLE ICONS HERE
}