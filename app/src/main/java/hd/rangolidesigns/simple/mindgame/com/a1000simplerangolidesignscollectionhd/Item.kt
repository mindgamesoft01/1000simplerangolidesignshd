package hd.rangolidesigns.simple.mindgame.com.a1000simplerangolidesignscollectionhd

 data class Item(
         val topic_title:String = "Topic Title",
         val topic_icon:String = "",
         val menus:ArrayList<String>
  )
